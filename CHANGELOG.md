CHANGELOG
=========

6.3.2 (17/02/2025)
------------------

- [Fix] tableAjaxData : lorsque la taille du tableau est indéfinie, on retourne 10000 éléments max.



6.3.1 (22/01/2025)
------------------

- Montée en version de Vite 5 => 6


6.3.0 (21/01/2025)
------------------

- Montée en version de plusieurs dépendances, dont Axios qui pouvait poser des PB de sécurité


6.2.7 (08/01/2025)
------------------

- [Fix] Les composants Vue sont aussi chargés à l'intérieur de div imbriquées de HTML renvoyé en Ajax


6.2.6 (07/01/2025)
------------------

- [Fix] Les composants Vue sont aussi chargés à l'intérieur de HTML renvoyé en Ajax


6.2.5 (19/09/2024)
------------------

- [fix] Les mois s'affichent maintenant bien dans le composant Calendar


6.2.4 (26/06/2024)
------------------

- [fix] le fromPost de la partie serveur Axios ne renvoie plus de stdClass, mais des array partout


6.2.3 (05/04/2024)
------------------

- Préservation des indexs de tableau avec L'Axios Extractor

6.2.2 (04/04/2024)
------------------

- Utilisatioh de v-model pour UTableAjax

6.2.1 (29/03/2024)
------------------

- Nouveau composant UTableAjax permettant de créer un tableau avec chargement des données en Ajax et affichage avec Vue, avec son côté serveur.
- Suppression de la fonction de formatage de date

6.2.0 (26/03/2024)
------------------

- Mise à niveau des versions des dépendances
- Intégration de BootstrapVueNext


6.1.3 (12/03/2024)
------------------

- Ajout d'une fonction de formatage de date à unicaenVue

6.1.2 (22/02/2024)
------------------

- [FIX] Les composants peuvent être placés dans des sous-sous répertoires de front

6.1.1 (21/02/2024)
------------------

- Nouvelles propriétés pour le composants génériques u-icon (id et rotate)
- Permettre la surcharge des options des toasts du flashmessenger

6.1 (11/12/2023)
------------------

- Passage à PHP8.2

6.0.0 (09/06/2023)
------------------
