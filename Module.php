<?php

namespace UnicaenVue;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent;

/**
 *
 *
 * @author Laurent LECLUSE <laurent.lecluse at unicaen.fr>
 */
class Module implements ConfigProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {

    }



    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

}
