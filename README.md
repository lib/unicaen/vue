# Unicaen/Vue

L'objectif de ce module est de proposer des outils pour utiliser Vue.js avec quelques outils connexes 
et intégrer le tout à l'écosystème Unicaen.

- [Liste des changements](CHANGELOG.md)
- [Installation](doc/install.md)
- [Documentation](doc/doc.md)

## Prérequis

- PHP 8
- Bootstrap 5
- BootstrapVueNext
- Node.js (sur les machines de développement uniquement : pas en production)
- Laminas



## Outils embarqués

### [Vue.js](https://vuejs.org/)

Version 3

Vue est un framework JavaScript pour la construction d'interfaces utilisateur. 
Il s'appuie sur HTML, CSS et JavaScript standards et fournit un modèle de programmation déclaratif 
et basé sur les composants qui vous aide à développer efficacement des interfaces utilisateur, 
qu'elles soient simples ou complexes.

### [Vite](https://vitejs.dev)

Version 5

Vite est un outil de construction qui vise à fournir une expérience de développement plus rapide et plus épurée 
pour les projets web modernes. Il se compose de deux parties majeures :

- Un serveur de développement qui offre des améliorations de fonctionnalités riches par rapport aux modules ES 
natifs, par exemple le remplacement de module à chaud (HMR ou hot-loading) extrêmement rapide.
- Une commande de construction qui regroupe votre code avec Rollup, pré-configuré pour produire des ressources 
statiques hautement optimisées pour la production.

Vite est extensible via son API de plugin et son API JavaScript avec un support de typage complet.

### [Axios](https://axios-http.com/)

Version 1

Axios est un client HTTP basé sur les promesses. 
Côté serveur, un plugin de contrôleur est proposé ainsi qu'un système permettant de construire des arborescences JSON 
et côté client (navigateur) il utilise les XMLHttpRequests.

### [BootstrapVueNext](https://bootstrap-vue-next.github.io/bootstrap-vue-next/docs)

Version alpha

Wrapper Bootstrap prêt pour Vue.js.
Avec BootstrapVue, vous pouvez construire des projets réactifs, mobiles-first et accessibles ARIA sur le web en utilisant Vue.js et la bibliothèque CSS front-end la plus populaire au monde.

BootstrapVueNext est une tentative d'avoir les composants BootstrapVue dans Vue 3, Bootstrap 5 et TypeScript. Un autre objectif est d'écrire les composants de manière simple et lisible pour offrir une meilleure expérience aux développeurs.