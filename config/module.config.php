<?php

namespace UnicaenVue;

use UnicaenVue\Controller\Plugin\AxiosPlugin;
use UnicaenVue\Controller\Plugin\AxiosPluginFactory;
use UnicaenVue\Controller\PublicController;
use UnicaenVue\Controller\Plugin\Axios;
use UnicaenVue\Controller\Plugin\AxiosFactory;
use UnicaenVue\View\Helper\ViteViewHelper;
use UnicaenVue\View\Helper\VueViewHelper;
use UnicaenVue\View\Renderer\AxiosRenderer;
use UnicaenVue\View\Renderer\AxiosRendererFactory;
use UnicaenVue\View\Renderer\VueRenderer;
use UnicaenVue\View\Renderer\VueRendererFactory;
use UnicaenVue\View\Strategy\AxiosStrategy;
use UnicaenVue\View\Strategy\AxiosStrategyFactory;
use UnicaenVue\View\Strategy\VueStrategy;
use UnicaenVue\View\Strategy\VueStrategyFactory;

return [
    'controller_plugins' => [
        'aliases'   => [
            'axios' => AxiosPlugin::class,
        ],
        'factories' => [
            AxiosPlugin::class => AxiosPluginFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            VueRenderer::class => VueRendererFactory::class,
            VueStrategy::class => VueStrategyFactory::class,
            AxiosRenderer::class => AxiosRendererFactory::class,
            AxiosStrategy::class => AxiosStrategyFactory::class,
        ],
    ],

    'view_helpers' => [
        'aliases'   => [
            'vite' => ViteViewHelper::class,
            'vue'  => VueViewHelper::class,
        ],
        'factories' => [
            ViteViewHelper::class => View\Helper\ViteViewHelperFactory::class,
            VueViewHelper::class  => View\Helper\VueViewHelperFactory::class,
        ],
    ],

    'view_manager' => [
        'strategies'               => [
            VueStrategy::class,
            AxiosStrategy::class,
        ],
    ],
];