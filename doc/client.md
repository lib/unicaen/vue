# Partie client en Javascript

UnicaenVue dispose d'un module Javascript conçu pour être utilisé sur la partie front des applications.

Il est accessible depuis un composant Vue aussi bien que depuis n'importe quel autre script Javascript sans avoir à utiliser `import` : il est préchargé et directement utilisable.

Pour y accéder, il suffit d'utiliser `window.unicaenVue` ou tout simplement `unicaenVue`.

Ce module propose deux choses pour le moment :
- une méthode url
- une instance augmentée d'Axios

## Gestion des Urls

Il est souvent nécessaire de devoir construire des URLs avec des paramètres de route et/ou des paramètres GET.

La méthode `unicenVue.url` fonctionne de la même manière que l'aide de vue url de Laminas : elle prend une route
en premier paramètre, un objet de paramètres en tant que paramètres de routes, puis éventuellement un second objet de paramètres pour le GET.

A partir de ça, elle va construire une URL sur le modèle suivant :

URL = page Home de l'application + route avec remplacement des variables par les paramètres de route + ? + paramètres GET (s'il y en a)

En voici quelques exemples d'utilisation :

```javascript
$(() => {

    // Dans ces exemple, la page home sera sur /
    // Dans les routes transmises, il ne faut du coup jamais commenter par "/".

    const url1 = unicaenVue.url('exemple/test');
    // affiche /exemple/test
    console.log(url1);

    const url2 = unicaenVue.url('exemple/test/:monId', {monId: 50});
    // affiche /exemple/test/50
    console.log(url2);

    const url3 = unicaenVue.url('exemple/test', {}, {monId: 50});
    // affiche /exemple/test?monId=50
    console.log(url3);

    const url4 = unicaenVue.url('exemple/test/:monId', {monId: 50}, {monCode: 'cinquante'});
    // affiche /exemple/test/50?monCode=cinquante
    console.log(url4);

})
```


## Axios

UnicaenVue fournit une instance personnalisée d'Axios.
Axios permet de faire de la communication Ajax entre le serveur et le navigateur.
Il peut être utilsé depuis des composants Vue, mai aussi ailleurs dans l'application.

UnicaenVue propose un Axios augmenté des possibilités suivantes :

- Une partie serveur d'Axios, intégrée au mode de fonctionnement de Laminas permettant de
  - traiter des données pour envoi au client avec un système d'extraction présenté ci-dessous
  - récupérer les messages du flashMessenger pour les exposer au client.

- Une partie client qui ajoute à l'existant la récupération des messages du flashMessenger envoyés par le serveur
  et les afficher sur la page sous formes d'alertes.

Pour le reste, [l'usage standard d'Axios](https://axios-http.com/fr/docs/intro) reste de mise

L'instance d'Axios est récupérable en appelant `unicaenVue.axios`.


Voici un exemple d'utilisation d'axios:

```javascript

unicaenVue.axios.get(
    // on construit l'Url nécessaire pour intérroger le serveur
    unicaenVue.url("exemple/test-data/:monId", {monId: this.monId})
).then(response => {
    // response.data contient les données
    console.log(response.data);
});
```

Lorsqu'il reçoit une réponse du serveur, si cette dernière comporte des messages issus du flashMessenger, il va les afficher sous formes de toasts.
Les toasts sont personnalisées selon qu'il s'agit de success, warning, info ou error. Ils sont affichés pendant 3 secondes, sauf les erreurs qu'il faut fermer 
manuellement afin d'avoir le temps de lire le message et de le traiter.

## Bibliothèque de composants personnalisés

UnicaenVue embarque quelques composants que vous pourrez utiliser directement, car il embarque un système d'autoloading de composants basé sur unplugin-vue-component.

Voici les composants directement utilisables :

- UCalendar : calendrier responsive utilisable sur smartphone avec possibilité de définir ses propres composants pour les événements
- UDate : Affiche de manière lisible une date fournie en String au format ISO ou en objet Date JS.
- UIcon : affiche une icône de font-awesome en ne lui donnant que le nom, sans avoir à écrire "fas fa-".
- UModal : dessine une fenêtre modale Bootstrap, vous n'avez plus qu'à écrire le contenu et les entêtes/pied de page.

En attendant une doc plus détaillée, vous pouvez voir les composants [ici](../components).

## Créer votre propre resolver

Si vous avez des composants génériques à utiliser un peu partout et que vous ne voulez pas faire d'import à tout bout de champ, il vous faut mettre en place votre propre résolveur.
C'est faisable facilement, il suffit de les mettre dans un répertoire, et de déclarer le tout dans la config de Vite.

```javascript
import unicaenVue from 'unicaen-vue';
    
...

const components = unicaenVue.findComponents('/var/www/mon/repertoire/absolu');

...
    
// bout de config Vite à mettre dans vite.config.js

export default unicaenVue.defineConfig({
  ...
  resolvers: [
    (componentName) => {
      if (components[componentName] !== undefined) {
        return components[componentName];
      }
    },
  ]
});
```