# Composants proposés en standard

- [UCalendar]() Calendrier pour la saisie d'informations par mois (à documenter)
- [UTableAjax](composants/UTableAjax.md) Permet d'afficher des tableaux de données chargés en Ajax et formatés avec Vue.js
