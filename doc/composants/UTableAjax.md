# UTableAjax

Permet de dessiner un pseudo DataTable avec un chargement des données en Ajax et un affichage en VueJS.

Exemple d'utilisation côté client, dans un composant Vue :

```vue
<template>
    <u-table-ajax :data-url="this.dataUrl" v-model="lines">
        <thead>
        <tr>
            <th column="ID">Id</th><!-- l'attribut column doit être renseigné pour pouvoir la rendre triable -->
            <th column="LIBELLE">Libellé</th>
            <th column="FORMULE">Formule</th>
            <th column="ANNEE">Année</th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <!-- On liste toute les lignes et on les affiche ici -->
        <tr v-for="(line,i) in lines" :key="i">
            <td>{{ line['ID'] }}</td>
            <td>{{ line['LIBELLE'] }}</td>
            <td>{{ line['FORMULE'] }}</td>
            <td>{{ line['ANNEE'] }}</td>
            <td><a :href="editUrl(line['ID'])">Modifier</a></td>
        </tr>
        </tbody>
    </u-table-ajax>
</template>

<script>

export default {
    name: 'Test',
    data()
    {
        return {
            dataUrl: unicaenVue.url('.../data'),
            
            lines: [],
        };
    },
    methods: {
        editUrl(id)
        {
            return unicaenVue.url('mon-url/:id', {id: id});
        },
    },
}

</script>
<style scoped>

</style>
```

Exemple de génération de données côté serveur, dans un contrôleur :
```php
<?php

namespace MonModule\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenVue\Util;

class TestController extends AbstractActionController
{

    public function dataAction()
    {
        // Requête SQL
        // Elle doit avoir un paramètre :search pour traiter les recherches
        // Elle ne doit pas avoir d'OrderBY : il sera ajouté ensuite selon le besoin
        $sql = "
        SELECT 
          fti.id      id, 
          fti.libelle libelle,
          f.libelle   formule,
          a.libelle   annee
        FROM 
          formule_test_intervenant fti
          JOIN formule f ON f.id = fti.formule_id
          JOIN annee a ON a.id = fti.annee_id
        WHERE
          lower(fti.libelle || ' ' || f.libelle || ' ' || a.libelle) like :search
        ";

        // renvoie un axiosModel avec les données issues du requêtage
        $em = /* Récupération de l'entityManager de Doctrine */;
        
        return Util::tableAjaxData($em, $this->axios()->fromPOst(), $sql);
    }
}
```

Résultat :

![UTableAjax](UTableAjax.png)