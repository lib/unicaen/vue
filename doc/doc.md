# Documentation du module Unicaen/Vue

[Procédure d'installation](install.md)

[Premier tuto](tuto1.md)

[Partie client Javascript](client.md)

[Partie serveur PHP](serveur.md)

[Composants proposés en standard](composants.md)

Pour faire vos premiers pas avec Vue :

- [Doc d'introduction à VueJS de Stéphane](https://git.unicaen.fr/bouvry/presentation-dev/-/blob/master/src/vuejs.md)

Sites officiels :

- [Vue.js](https://vuejs.org/)
- [Vite](https://vitejs.dev)
- [Axios](https://axios-http.com/)
- [BootstrapVueNext](https://bootstrap-vue-next.github.io/bootstrap-vue-next/docs)