# Installation d'UnicaenVue

L'installation & la configuration du module se font en 5 étapes.

## 1. Composer

UnicaenVue est disponible via le Packagist, comme les autres bibliothèques Unicaen.

```composer log
    ...
    "require"          : {
        ...
        "unicaen/vue"                                 : "6.0.0",
        ...
    },
    ...
```

Lancez `composer update`

## 2. Module Laminas

UnicaenVue est un module Laminas.

Il faut donc l'ajouter à la liste des modules de votre application.

```php
$modules = [
    ...
    'UnicaenVue',
    ...
];
```

Il faut aussi dupliquer dans config/autoload et paramétrer les fichiers de configuration suivants :
 - [unicaen-vue.local.php](../config/unicaen-vue.local.php.dist)
 - [unicaen-vue.global.php](../config/unicaen-vue.global.php.dist)

## 3. Node.js

UnicaenVue est aussi un module Node.

Après avoir installé Node & NPM sur la machine de développement, il faut ajouter la dépendance au fichier package.json à la racine du répertoire du projet.

Les scripts de dev et de build peuvent ausi être utiles. dev sert à lancer
le serveur Node pour le hot-loading, et build permet de compiler le front pour intégration au GIT et mise en production.

Voici un exemple complet de fichier package.json :

```json
{
  "name": "ose",
  "version": "21.0.0",
  "author": "Université de Caen Normandie",
  "description": "Organisation des Services d'Enseignement",
  "license": "ISC",
  "private": true,
  "scripts": {
    "dev": "APP_ENV=development vite dev",
    "build": "vite build"
  },
  "dependencies": {
    "unicaen-vue": "file:./vendor/unicaen/vue"
  }
}

```

Lancez `npx npm update` depuis le répertoire racine de votre application.


## 4. Vite

Un fichier de configuration pour Vite est également à ajouter dans le répertoire racine du projet, dans le fichier `vite.config.js`.

En voici un exemple :

```javascript
import unicaenVue from 'unicaen-vue';
import path from 'path';

/**
 * @see https://vitejs.dev/config/
 *
 * la config transmise ci-dessous est surchargée par UnicaenVue.defineConfig, qui ajoute ses propres éléments
 * puis retourne vite.defineConfig
 */
export default unicaenVue.defineConfig({
    // répertoire où seront placés les fichiers *.vue des composants
    root: 'front',
    build: {
        // Répertoire où seront placés les fichiers issus du build et à ajouter au GIT
        // à mettre en cohérence avec la config côté PHP (unicaen-vue/dist-path)
        outDir: path.resolve(__dirname, 'public/dist'),
    },
    server: {
        // port par défaut utilisé par Node pour communiquer les éléments en "hot-loading"
        // utile uniquement en mode dev, donc
        port: 5133
    },
    resolvers: [
        // Liste de resolvers pour faire de l'auto-import
    ],
});
```

Il n'est pas nécessaire de le modifier, à moins que vous ne souhaitiez ajouter des modules à Vite ou personnaliser votre configuration.

## 5. Répertoire front & fichier main.js

Créez un nouveau répertoire `front` à la racine de votre projet.
Ce répertorie contiendra tous vos composants Vue.

Un fichier `main.js` doit être aussi créé. Il sert à initialiser la partie front de l'application.

En voici le contenu :
```javascript
const vues = import.meta.glob('./**/*.vue', {eager: true});
import vueApp from 'unicaen-vue/js/Client/main'

vueApp.init(vues);
```

## 6. Lancement du front depuis le layout

Enfin, dernière étape, vous devez démarrer la partie "front".
Pour cela, vous devrez ajouter Vite au layout de votre application, dans le HEAD :
```html
<html lang="fr">
<head>
    ...
    <?= $this->vite(); ?>
    ...
</head>
<body>
...

```


lancer Vite en mode dev pour bénéficier du hot-loading:

 - `npx vite dev`

C'est prêt!!!
Il ne reste plus qu'à créer votre premier composant.

Un premier tuto est disponible [ici](tuto1.md)

Une fois vos développements terminés, compilez et commitez le tout :

- `npx vite build` pour compiler