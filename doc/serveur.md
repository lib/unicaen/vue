# Utiliser UnicaenVue depuis le serveur en PHP

## Instancier un composant Vue depuis une action de contrôleur

Depuis une action de contrôleur, vous pouvez renvoyer directement un `VueModel`, qui va se charger
d'instancier un composant Vue directement sans avoir à passer par une `view` en PHP.

Le fonctionnement est très proche du `ViewModel` de Laminas, à ceci près que :
- Les variables transmises sont converties en JSON pour être passées au client. Il est donc impossible de passer un service ou autre.
Vous pouvez passer en revanche des entités ou des tableaux, des variables telles que des entiers, flotants, chaines, dates (`\DateTime`).
La convertion en Json se fait à l'aide de l'[AxiosExtractor](#axiosextractor). Pour avoir la liste précise des types des données gérés rendez-vous [ci-dessous](#axiosextractor), donc.

- Le template fourni est en fait un chemin pointant vers un composant Vue. La syntaxe est très proche de celle des templates utilisés pour 
les `view` : `chemin/vers/le/composant/nom-du-composant`, en kebab-case donc!
Exemple pour un composant `MonTest` dont le fichier est placé dans `front/Exemple/MonTest.vue` : `exemple/mon-test`



Exemple :

```php

Dans un contrôleur :

// On utilise VueModel 
use UnicaenVue\View\Model\VueModel;
...

    // Action qui va charger le composant MonTest
    public function testAction()
    {
        $data = [
            'monId' => 50,
        ];

        // On crée un VueModel et on lui transmet des données par défaut
        $vm = new VueModel($data);

        // On lui donne un template, c'est-à-dire le chemin et le nom du composant à utiliser
        // Le composant s'appelle MonTest se trouvera dans le fichier front/Exemple/MonTest.vue
        // Notez qu'on utilise par convention une syntaxe kebab-case pour le template et CamelCase pour les répertoires, & noms de composants Vue.
        $vm->setTemplate('exemple/mon-test');

        // On retourne le VueModel
        return $vm;
    }
...
```


## Instancier un composant Vue depuis une vue

Lorsque c'est nécessaire, il est aussi possible d'intégrer un composant Vue à une `view` existante.
L'aide de vue `vue` est là pour ça.

En voici un exemple :

```php

// instancie le composant Liste placé dans le fichier front/Mission/Liste.vue
echo $this->vue('mission/liste', [
    // on lui passe un entier en ID, ce qui va permettre de récupérer l'ensemble des données de l'intervenant plus tard gràce à Axios
    'intervenant'   => $intervenant->getId(),
    // Et on lui transmet aussi un booleen
    'canAddMission' => $canAddMission,
]);
```

Autre exemple, si vous voulez afficher plusieurs composants Vue d'affilée :

```php

// on démarre une vue-app
echo $this->vue()->begin();


// affichage des composants
echo $this->vue()->component('monPremierComposant', [/* tableau de propriétés */]);
echo $this->vue()->component('monSecondComposant', [/* tableau de propriétés */]);

// on termine la vue-app
echo $this->vue()->end();

```

## Utilisation d'Axios

### Récupération de données en POST

Lorsqu'au niveau du client on lance `axios.post` les données ne sont pas récupérables avec le plugin de contrôleur `params`,
car Axios encode les données en `application/x-www-form-urlencoded`.

Un plugin de contrôleur spécifique nommé... `axios` a donc été mis en place.

Avec Axios, donc, plutôt que

```php
$inputData = $this->params()->fromPost();
```

utilisez à la place

```php
$inputData = $this->axios()->fromPost();
```

### Envoi de données via Axios

Les données renvoyées doivent être encapsulées dans un `AxiosModel`.
Ce dernier va :
- utiliser [AxiosExtractor](#axiosextractor) pour convertir les données
- collecter les éventuels messages issus du `flashMessenger`
- convertir le tout en Json pour envoi au client

AxiosModel peut prendre en construction jusqu'à 3 paramètres (facultatifs):
1. Les données en tant que telles
2. Les propriétés (utiles seulement si on traite des objets et facultatives si on transmet des objets implémentant AxiosInterface)
3. Les triggers (si nécessaire). Vous trouverez [plus d'infos sur les triggers ici](#les-triggers).

Voici un exemple d'action de contrôleur qui envoie des données au client en mode Ajax vers Axios :

```php
use UnicaenVue\View\Model\AxiosModel;
...

    // exemple d'une action de contrôleur renvoyant des données au client suite à une requête Axios
    public function testDataAction(){
        // on récupère les données sur la base de l'ID récupéré
        $contacts = [
            50 => [
                ['nom' => 'monNom', 'prenom' => 'monPrenom'],
                ['nom' => 'monNom2', 'prenom' => 'monPrenom2'],
                ['nom' => 'monNom3', 'prenom' => 'monPrenom3'],
            ],
        ];

        // Petit message d'info à afficher sous forme de Toast
        $this->flashMessenger()->addSuccessMessage('Tout va bien!');

        // Et on retourne un AxiosModel qui présente le tout au client
        // Ici, nous traitons un tableau en entrée, donc pas besoin de préciser des propriétés à extraire et nous n'avons pas besoin de triggers
        return new AxiosModel($contacts[$monId]);
        
        // Si on exporte un objet ou un tableau d'objets, on peut aussi fournir une liste de propriétés, cf. ExiosExtractor
        // $proprietes = [/* liste des propriétés à exporter */];
        // => return new AxiosModel($monObjet, $proprietes);
    }

```

Si vous n'avez besoin de ne renvoyer que le premier item d'une liste et que l'AxiosModel renvoie une liste,
alors vous pouvez faire comme dans l'exemple suivant :

```php

// On crée dans $query une requête de type Doctrine\Orm\Query
$entityManager = ... // Votre entityManager
$dql = "... votre requête DQL";
$query = $entityManager->createQuery($dql);

// Création du modèle Axios
$model = new AxiosModel;

// on peut aussi passer les données (ou Query) et les propriétés avec des accesseurs
$model->setData($query);
$model->setProperties($properties);

$properties = [
    ... // La liste de vos propriétés à retourner
];

// La ligne ci-dessous fera que l'AxiosRenderer ne renverra que le premier item du résultat de votre requête
$model->returnFirstItem();

return $model;

```

## AxiosExtractor

L'AxiosExtractor est chargé de convertir des données en PHP issues du serveur afin de pouvoir les ré-exploiter sur le client, en Javascript.

Il n'a pas besoin d'être appelé explicitement, hors cas d'usage spécifique. 

Il est appelé en arrière-plan lorsque vous retournez un `AxiosModel`, un `VueModel` ou faites appel à l'aide de vue `vue`.
En temps normal, vous pourrez vous borner à retourner des AxiosModel dans des actions de contrôleurs.

Au besoin, il faudra appeler la méthode globale `\UnicaenVue\Axios\AxiosExtractor::extract`.

### Types simples

Il peut gérer les types de données simples et des types plus complexes comme les entités ou même des `query` Doctrine.
En revanche, il ne sera pas en mesure de traiter des services ou autres objets non utilisables hors du contexte serveur.

Les types de données simples sont supportés, et une fois récupérés sur le client, le typage est préservé c'est-à-dire qu'un entier reste un entier, un boolean reste un booléen, etc.

```php
$data = 15;
$extracted = \UnicaenVue\Axios\AxiosExtractor::extract($data);
var_dump($extracted); // => int 15
// idem pour string, bool, float
```

Pour les dates, les données de type \DateTime sont converties en chaînes de caractère au standard ISO 8601 ('Y-m-d\TH:i:s.u\Z') préconisé pour HTML5.

```php
$data = new \DateTime();
$extracted = \UnicaenVue\Axios\AxiosExtractor::extract($data);
var_dump($extracted); // => string '2023-03-29T15:58:19.368432Z'
```

### Tableaux

Les tableaux PHP sont également gérés et transformés en objets JSON (`{k1: 'v1', k2: 'v2'}`) ou en listes JSON `['v1','v2']`.
Ils sont parcourus de manière récursive et ne doivent bien entendu pas intégrer de données ayant des types non gérables.

côté PHP :
```php
$data = [
    'nom' => 'Dupont',
    'prenom' => 'Robert',
    'adresse' => [
        'numéro' => 1,
        'rue' => 'Allée des mésanges',
        'cp' => 14000,
        'ville' => 'Caen',
        'france' => true,
    ],
];
$extracted = \UnicaenVue\Axios\AxiosExtractor::extract($data);
var_dump($extracted); // $extracted = $data
```
côté JS :
```javascript
    const result = <?= json_encode($extracted) ?>;
    console.log(result);
    /* renvoie (idem dans response.data d'Axios) :
    { 
        nom: "Dupont", 
        prenom: "Robert", 
        adresse: {
            cp: 14000
            france: true
            "numéro": 1
            rue: "Allée des mésanges"
            ville: "Caen"
        } 
    }
    
    */
})
```

### Entités & objets

Les Entités ou autres objets portant des données sont également gérables. Leur extraction est, contrairement aux types simples, paramétrable.

Prenons les deux entités suivantes en exemple :

```php
class Adresse {
    protected int $numero = 1;
    protected string $rue = 'Allée des mésanges';
    protected int $cp = 14000;
    protected string $ville = 'Caen';

    public function getNumero(): int
    {
        return $this->numero;
    }

    public function getRue(): string
    {
        return $this->rue;
    }

    public function getCp(): int
    {
        return $this->cp;
    }

    public function getVille(): string
    {
        return $this->ville;
    }
}

class Personne
{
    protected string $nom = 'Dupont';
    protected string $prenom = 'Robert';
    protected int $age = 42;
    protected Adresse $adresse;

    public function __construct()
    {
        $this->adresse = new Adresse();
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function getAge(): int
    {
        return $this->age;
    }

    public function getAdresse(): Adresse
    {
        return $this->adresse;
    }

    public function isMajeur(): bool
    {
        return $this->age >= 18;
    }
}

```

#### Extraction avec des propriétées données explicitement

Et voici une première extraction de données :

On précise ici les données à exporter en listant les propriétés concernées.

```php
$personne = new Personne();

$properties = ['nom', 'prenom', 'isMajeur', ['adresse', ['cp', 'ville']]];
$extracted = \UnicaenVue\Axios\AxiosExtractor::extract($personne,$properties);
var_dump($extracted);
// Renvoie :
// array (size=4)
//  'nom' => string 'Dupont' (length=6)
//  'prenom' => string 'Robert' (length=6)
//  'isMajeur' => boolean true
//  'adresse' => array (size=2)
//      'cp' => int 14000
//      'ville' => string 'Caen' (length=4)
```

Les propriétés doivent avoir des accesseurs, par exemple pour la propriété age, l'entité doit avoir une fonction parmis les suivantes :
- age()
- getAge()
- isAge()
- hasAge()

Pour l'adresse, comme il s'agit d'une entité, on doit fournir un tableau ayant deux valeurs : `['nom de la propriété', [...liste des propriétés de la sous-entité]]`.

#### Extraction sur un objet implémentant AxiosExtractorInterface

Si des propriétés sont explicitement transmises en second paramètre d'extract, alors elles sont utilisées en priorité.
A défaut, AxiosExtractor peut extraire la liste des propriétés si les objets implémentent `UnicaenVue\Axios\AxiosExtractorInterface`

Cela peut être utile pour des objets dont nous avons toujours besoin de renvoyer les mêmes propriétés.
Dans la mesure du possible, évitez de l'utiliser et privilégiez la fourniture explicite de "properties" au moment de l'extraction de données.
Cela facilitera la compréhension de votre code.

A l'exemple ci-dessus, on ajoute à la classe Adresse :

```php
class Adresse implements \UnicaenVue\Axios\AxiosExtractorInterface
{
   ...

    public function axiosDefinition(): array
    {
        // par défaut, les extractions de Adresse auront donc les deux propriétés cp et ville
        return ['cp', 'ville'];
    }
}
```

Et toujours en reprenant l'exemple ci-dessus, avec cette modification, le code suivant fonctionnera :

```php
$personne = new Personne();

$properties = ['nom', 'prenom', 'isMajeur', 'adresse'];
$extracted = \UnicaenVue\Axios\AxiosExtractor::extract($personne,$properties);
var_dump($extracted);
// Renvoie :
// array (size=4)
//  'nom' => string 'Dupont' (length=6)
//  'prenom' => string 'Robert' (length=6)
//  'isMajeur' => boolean true
//  'adresse' => array (size=2)
//      'cp' => int 14000
//      'ville' => string 'Caen' (length=4)
```

#### Les triggers

Il est possible de transformer les données traitées via Axios au moyen de triggers.
Ce sera utile si, par exemple, on veut ajouter des propriétés à la volée à un objet qui n'en était pas pourvu ou si on veut modifier 
après coup la valeur de certaines données.

Il s'agit d'un usage avancé du dispositif.
A n'utiliser, donc, qu'à défaut de meilleure solution.
L'idéal est de fournir des entités créées spécifiquement pour répondre à vos besoins d'extraction.

Les triggers sont un tableau de fonctions dont les clés sont les chemins qui vont permettre de déterminer sur quelles données nous
voudrons agir, et les valeurs sont des fonctions anonymes dotées de deux paramètres $original : les données originales, c'est-à-dire avant extractions et $extracted, c'est-à-dire le résultat extrait de l'original.
La fonction anonyme devra renvoyer un résultat qui remplacera $extracted dans l'arborescence finale.

Les chemins sont définis de la manière suivante :
 * Si on a une liste d'objets, alors `'/'` permettra d'atteindre chaque occurence du tableau renvoyé.
 * Si on y ajoute `[]`, ce qui donnera `'/[]'`, alors on pourra agir sur tout le tableau de données (à condition toutefois que ce soit bien une liste de données qu'on manipule).
 * Si on veut accéder à des données plus en avant dans l'arborescence, alors on la parcourera avec des slashs `/``, comme des répertoires.

Exemple de chemin plus complexe : prenons la structure de données citée [ci-dessous](#depuis-doctrine).
Si nous voulons agir sur les créateurs des volumes horaires, il faudra utiliser le chemin suivant :
`/volumesHoraires/histoCreateur`.
Nous pourrons ainsi traiter les créateurs des volumes horaires, et pas les créateurs des missions.



Exemple d'utilisation des triggers :

Prenons la structure de données ci-dessus, à savoir une liste de personnes ayant une adresse.

Voici un exemple de trigger qui va ajouter l'âge aux personnes :

```php
$personnes = [new Personne(), new Personne()];

$properties = ['nom', 'prenom', 'isMajeur', 'adresse'];

$triggers = [
    // '/' signifie que nous agirons sur les données de premier niveau, qui sont ici des Personne. Le trigger agira pour chaque personne
    '/' => function($original, $extracted){
        // $original contiendra l'objet correspondant à l'entité Personne
        // $extracted contiendra le tableau de données déjà extrait
        
        $extracted['age'] = 37; // On ajoute ici une propriété en extraction qui n'a pas été générée avant.
        // Nous pourrions tout aussi bien retirer une donnée, ou bien en changer le type ou la valeur.
        
        return $extracted;
    },
    
    // Autre exemple de trigger qui agira non plus sur chaque personne de la liste, mais sur l'ensemble de la liste
    '/[]' => function($original, $extracted){
        // $original contient la liste des objets Personne
        // $extracted contient un tableau des deux personnes converties en array
        
        unset($extracted[0]);// on supprime la première personne de la liste
        
        // il est obligatoire de retourner quelque chose
        return $extracted;
    },
    
    // Encore un autre exemple : on va agir sur les adresses de chaques personnes
    '/adresse' => function($original, $extracted){
        $extracted['pays'] = 'France';
        
        return $extracted;
    }
];

$extracted = \UnicaenVue\Axios\AxiosExtractor::extract($personnes,$properties,$triggers);
var_dump($extracted);
// Renvoie :
// array (size=4)
//  'nom' => string 'Dupont' (length=6)
//  'prenom' => string 'Robert' (length=6)
//  'isMajeur' => boolean true
//  'adresse' => array (size=2)
//      'cp' => int 14000
//      'ville' => string 'Caen' (length=4)
```


### Depuis Doctrine

Axios peut également prendre en entrée des requêtes Doctrine `Doctrine\ORM\Query`.

Exemple : 

```php

// Dans une action de contrôleur :
// On crée une requête DQL qui va chercher toutes les données dont nous avons besoin
// L'objectif est ici de récupérer une liste de missions 
$dql = "
    SELECT 
      m, tm, str, tr, valid, vh, vvh, ctr
    FROM 
      " . Mission::class . " m
      JOIN m.typeMission tm
      JOIN m.structure str
      JOIN m.tauxRemu tr
      JOIN " . TypeVolumeHoraire::class . " tvh WITH tvh.code = :typeVolumeHorairePrevu
      LEFT JOIN m.validations valid WITH valid.histoDestruction IS NULL
      LEFT JOIN m.volumesHoraires vh WITH vh.histoDestruction IS NULL AND vh.typeVolumeHoraire = tvh
      LEFT JOIN vh.validations vvh WITH vvh.histoDestruction IS NULL
      LEFT JOIN vh.contrat ctr WITH ctr.histoDestruction IS NULL
    WHERE 
      m.histoDestruction IS NULL 
    ORDER BY
      m.dateDebut,
      vh.histoCreation
    ";

    $parameters = [
        'typeVolumeHorairePrevu' => TypeVolumeHoraire::CODE_PREVU,
    ];

    $query = $this->getEntityManager()->createQuery($dql)->setParameters($parameters);

    // On donne $query, de type Doctrine\ORM\Query à l'AxiosModal, qui la transmet à l'AxiosExtractor pour traitement
    // Ici, les propriétés ne sont pas définies, dont l'entité Mission doit implémenter `UnicaenVue\Axios\AxiosExtractorInterface`
    // Idem pour les autres entités remontées.
    // Les donnnées transmises au client seront une liste de missions, avec toutes les données et sous-entités afférentes converties en objets JSON
    
    // les properties sont définies ici    
    $utilisateurProperties = ['id','email','displayName'];
    
    $properties = [
        "id",
        // Pour le type de mission, on ne renverra que "id" et "libelle"
        ['typeMission',['id','libelle']],
        'dateDebut',
        'dateFin',
        ['structure',['id','libelle']],
        ['tauxRemu',['id','libelle']],
        'description',
        'histoCreation',
        ['histoCreateur',$utilisateurProperties],
        'heures',
        'heuresValidees',
        ['volumesHoraires',[
            'id',
            'heures',
            'valide',
            'validation',
            'histoCreation',
            ['histoCreateur',$utilisateurProperties],
            'canValider',
            'canDevalider',
            'canSupprimer',
        ]],
        'contrat',
        'valide',
        'validation',
        'canSaisie',
        'canValider',
        'canDevalider',
        'canSupprimer',
    ];

    return new AxiosModel($query, $properties);
```

Et voici le résultat final récupéré dans le composant Vue avec Axios :
```json
[
    {
        "id"             : 36,
        "typeMission"    : {
            "id"     : 316,
            "libelle": "Référent étudiant en résidence universitaire Crous"
        },
        "dateDebut"      : "1980-02-02T00:00:00.000000Z",
        "dateFin"        : "1980-03-03T00:00:00.000000Z",
        "structure"      : {
            "id"     : 173,
            "libelle": "Dir Communication"
        },
        "tauxRemu"       : {
            "id"     : 1,
            "libelle": "Taux 1"
        },
        "description"    : "dd",
        "histoCreation"  : "2023-02-01T13:53:04.000000Z",
        "histoCreateur"  : {
            "id"         : 999999999,
            "email"      : "laurent.lecluse@unicaen.fr",
            "displayName": "Laurent Local"
        },
        "heures"         : 60,
        "heuresValidees" : 0,
        "volumesHoraires": [
            {
                "id"           : 15,
                "heures"       : 50,
                "valide"       : false,
                "validation"   : null,
                "histoCreation": "2023-02-01T13:53:04.000000Z",
                "histoCreateur": {
                    "id"         : 999999999,
                    "email"      : "laurent.lecluse@unicaen.fr",
                    "displayName": "Laurent Local"
                },
                "canValider"   : true,
                "canDevalider" : false,
                "canSupprimer" : true
            },
            {
                "id"           : 16,
                "heures"       : 10,
                "valide"       : false,
                "validation"   : null,
                "histoCreation": "2023-02-01T14:08:35.000000Z",
                "histoCreateur": {
                    "id"         : 999999999,
                    "email"      : "laurent.lecluse@unicaen.fr",
                    "displayName": "Laurent Local"
                },
                "canValider"   : true,
                "canDevalider" : false,
                "canSupprimer" : true
            },
            {
                "id"           : 34,
                "heures"       : -1,
                "valide"       : false,
                "validation"   : null,
                "histoCreation": "2023-03-02T10:41:31.000000Z",
                "histoCreateur": {
                    "id"         : 999999999,
                    "email"      : "laurent.lecluse@unicaen.fr",
                    "displayName": "Laurent Local"
                },
                "canValider"   : true,
                "canDevalider" : false,
                "canSupprimer" : true
            },
            {
                "id"           : 35,
                "heures"       : 1,
                "valide"       : false,
                "validation"   : null,
                "histoCreation": "2023-03-02T10:41:50.000000Z",
                "histoCreateur": {
                    "id"         : 999999999,
                    "email"      : "laurent.lecluse@unicaen.fr",
                    "displayName": "Laurent Local"
                },
                "canValider"   : true,
                "canDevalider" : false,
                "canSupprimer" : true
            }
        ],
        "contrat"        : false,
        "valide"         : false,
        "validation"     : null,
        "canSaisie"      : true,
        "canValider"     : true,
        "canDevalider"   : false,
        "canSupprimer"   : true
    },
    {
        "id"             : 37,
        "typeMission"    : {
            "id"     : 1,
            "libelle": "Accompagnement des étudiants en situation de handicap"
        },
        "dateDebut"      : "1980-03-02T00:00:00.000000Z",
        "dateFin"        : "1980-03-03T00:00:00.000000Z",
        "structure"      : {
            "id"     : 173,
            "libelle": "Dir Communication"
        },
        "tauxRemu"       : {
            "id"     : 1,
            "libelle": "Taux 1"
        },
        "description"    : "Test",
        "histoCreation"  : "2023-02-02T09:49:45.000000Z",
        "histoCreateur"  : {
            "id"         : 999999999,
            "email"      : "laurent.lecluse@unicaen.fr",
            "displayName": "Laurent Local"
        },
        "heures"         : 80,
        "heuresValidees" : 80,
        "volumesHoraires": [
            {
                "id"           : 29,
                "heures"       : 80,
                "valide"       : true,
                "validation"   : {
                    "id"           : 269874,
                    "histoCreation": "2023-02-02T09:50:29.000000Z",
                    "histoCreateur": {
                        "id"         : 999999999,
                        "email"      : "laurent.lecluse@unicaen.fr",
                        "displayName": "Laurent Local"
                    }
                },
                "histoCreation": "2023-02-02T09:49:45.000000Z",
                "histoCreateur": {
                    "id"         : 999999999,
                    "email"      : "laurent.lecluse@unicaen.fr",
                    "displayName": "Laurent Local"
                },
                "canValider"   : false,
                "canDevalider" : false,
                "canSupprimer" : false
            }
        ],
        "contrat"        : false,
        "valide"         : true,
        "validation"     : {
            "id"           : 269874,
            "histoCreation": "2023-02-02T09:50:29.000000Z",
            "histoCreateur": {
                "id"         : 999999999,
                "email"      : "laurent.lecluse@unicaen.fr",
                "displayName": "Laurent Local"
            }
        },
        "canSaisie"      : false,
        "canValider"     : false,
        "canDevalider"   : true,
        "canSupprimer"   : false
    },
    {
        "id"             : 14,
        "typeMission"    : {
            "id"     : 2,
            "libelle": "Appui aux personnels des bibliothèques"
        },
        "dateDebut"      : "2022-01-01T00:00:00.000000Z",
        "dateFin"        : "2022-01-02T00:00:00.000000Z",
        "structure"      : {
            "id"     : 175,
            "libelle": "DEVE"
        },
        "tauxRemu"       : {
            "id"     : 1,
            "libelle": "Taux 1"
        },
        "description"    : "dvfdd",
        "histoCreation"  : "2023-01-26T10:53:19.000000Z",
        "histoCreateur"  : {
            "id"         : 999999999,
            "email"      : "laurent.lecluse@unicaen.fr",
            "displayName": "Laurent Local"
        },
        "heures"         : 12,
        "heuresValidees" : 0,
        "volumesHoraires": [
            {
                "id"           : 12,
                "heures"       : 12,
                "valide"       : false,
                "validation"   : null,
                "histoCreation": "2023-01-31T10:25:56.000000Z",
                "histoCreateur": {
                    "id"         : 999999999,
                    "email"      : "laurent.lecluse@unicaen.fr",
                    "displayName": "Laurent Local"
                },
                "canValider"   : true,
                "canDevalider" : false,
                "canSupprimer" : true
            }
        ],
        "contrat"        : false,
        "valide"         : false,
        "validation"     : null,
        "canSaisie"      : true,
        "canValider"     : true,
        "canDevalider"   : false,
        "canSupprimer"   : true
    },
    {
        "id"             : 31,
        "typeMission"    : {
            "id"     : 395,
            "libelle": "Tutorat au bénéfice des étudiants de première année"
        },
        "dateDebut"      : "2023-01-27T00:00:00.000000Z",
        "dateFin"        : "2023-01-27T00:00:00.000000Z",
        "structure"      : {
            "id"     : 173,
            "libelle": "Dir Communication"
        },
        "tauxRemu"       : {
            "id"     : 1,
            "libelle": "Taux 1"
        },
        "description"    : "Test de saisie de VH Prév",
        "histoCreation"  : "2023-01-27T16:06:35.000000Z",
        "histoCreateur"  : {
            "id"         : 999999999,
            "email"      : "laurent.lecluse@unicaen.fr",
            "displayName": "Laurent Local"
        },
        "heures"         : 15,
        "heuresValidees" : 0,
        "volumesHoraires": [
            {
                "id"           : 13,
                "heures"       : 15,
                "valide"       : false,
                "validation"   : null,
                "histoCreation": "2023-01-31T10:26:27.000000Z",
                "histoCreateur": {
                    "id"         : 999999999,
                    "email"      : "laurent.lecluse@unicaen.fr",
                    "displayName": "Laurent Local"
                },
                "canValider"   : true,
                "canDevalider" : false,
                "canSupprimer" : true
            }
        ],
        "contrat"        : false,
        "valide"         : false,
        "validation"     : null,
        "canSaisie"      : true,
        "canValider"     : true,
        "canDevalider"   : false,
        "canSupprimer"   : true
    }
]
```