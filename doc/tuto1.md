# Premier tuto Unicaen/Vue

L'objectif est de créer un premier composant, de le lancer à partir d'une action de contrôleur, puis de récupérer des données à l'aide d'Axios et les afficher

## 1. Partie serveur

Dans un contrôleur, créez deux actions :

```php

...
use UnicaenVue\View\Model\AxiosModel;
use UnicaenVue\View\Model\VueModel;
...

    // Action qui va charger le composant MonTest
    // route exemple/test à ajouter à votre config
    public function testAction()
    {
        $data = [
            'monId' => 50,
        ];

        // On crée un VueModel et on lui transmet des données par défaut
        $vm = new VueModel($data);

        // On lui donne un template, c'est-à-dire le chemin et le nom du composant à utiliser
        // Le composant s'appelle MonTest se trouvera dans le fichier front/Exemple/MonTest.vue
        // Notez qu'on utilise par convention une syntaxe kebab-case pour le template et CamelCase pour les répertoires, & noms de composants Vue.
        $vm->setTemplate('exemple/mon-test');

        // On retourne le VueModel
        return $vm;
    }



    // Action qui envoie des données au composant
    // route exemple/test-data/:monId à ajouter à votre config
    public function testDataAction()
    {
        $monId = $this->params()->fromRoute('monId');

        // on récupère les données sur la base de l'ID récupéré
        $contacts = [
            50 => [
                ['nom' => 'monNom', 'prenom' => 'monPrenom'],
                ['nom' => 'monNom2', 'prenom' => 'monPrenom2'],
                ['nom' => 'monNom3', 'prenom' => 'monPrenom3'],
            ],
        ];

        // Petit msg d'info
        $this->flashMessenger()->addSuccessMessage('Tout va bien!');

        // Et on retourne un AxiosModel qui présente le tout au client
        return new AxiosModel($contacts[$monId]);
    }
...
```


## 2. Partie client

Créez, dans le fichier front/Exemple/MonTest, le composant suivant :

```vue
<template>
    <p>Mon ID : {{ monId }}</p>

    <h2>Contacts</h2>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Prénom</th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="c in contacts">
            <td>{{ c.nom }}</td>
            <td>{{ c.prenom }}</td>
        </tr>
        </tbody>
    </table>
</template>

<script>
export default {
    name: "MonTest",
    props: {
        monId: {required: true, type: Number}
    },
    data()
    {
        return {
            contacts: []
        };
    },
    mounted()
    {
        // on utilise l'axios adapté d'UnicaenVue
        unicaenVue.axios.get(
            // unicaenVue.url permet de créer une URL à partir d'une route et de paramètres d'URL
            unicaenVue.url("exemple/test-data/:monId", {monId: this.monId})
        ).then(response => {
            // response.data contient les données, on met à jour les contacts avec...
            this.contacts = response.data;
        });
    }
}

</script>

<style scoped>

</style>
```