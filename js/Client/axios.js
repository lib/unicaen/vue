import axios from 'axios';
import flashMessenger from './flashMessenger';

// uvAxios est un clone du axops standard
let uvAxios = { ...axios};

/* Tunning d'Axios pour gérer l'interconnexion avec le serveur avec gestion des toasts */

// Permet d'afficher une animation de chargement dans un popover si l'objet submitter a été transmis
uvAxios.interceptors.request.use(config => {
    if (config.submitter) {
        let msg = config.msg ? config.msg : 'Action en cours';
        if (config.popover != undefined) {
            config.popover.dispose();
        }
        config.popover = new bootstrap.Popover(config.submitter, {
            content: "<div class=\"spinner-border text-primary\" role=\"status\">\n" +
                "  <span class=\"visually-hidden\">Loading...</span>\n" +
                "</div> " + msg,
            html: true,
            trigger: 'focus'
        });
        config.popover.show();
    }
    return config;
});

// On capte la réponse pour afficher le résultat si on avait un submitter, sinon on affiche des toasts
uvAxios.interceptors.response.use(response => {
    response.messages = response.data.messages;
    response.data = response.data.data;
    response.hasErrors = response.messages && response.messages.error && response.messages.error.length > 0 ? true : false;

    if (response.config.popover) {
        var popover = response.config.popover;

        let content = '';
        for (ns in response.messages) {
            for (mid in response.messages[ns]) {
                content += '<div class="alert fade show alert-' + (ns == 'error' ? 'danger' : ns) + '" role="alert">' + response.messages[ns][mid] + '</div>';
            }
        }

        // S'il y a un truc à afficher
        if (content) {
            popover._config.content = content;
            popover.setContent();
            setTimeout(() => {
                popover.dispose();
            }, 5000)
        } else {
            // la popover est masquée si tout est fini
            popover.dispose();
        }
    }
    if (response.messages) {
        flashMessenger.toasts(response.messages);
    }

    return response;
}, (error) => {
    let message = error.response.data;

    if (error.response.status == 403){
        message = '<h4>403 - Accès interdit</h4><br />Vous n\'êtes pas autorisé(e) à faire cette action.';
    }else{
        message = error.response.data;
    }

    flashMessenger.toast(message, 'error');
});

uvAxios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export default {
    uvAxios
}