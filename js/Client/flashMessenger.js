function toasts(messages) {
    for (let s in messages) {
        for (let m in messages[s]) {
            toast(messages[s][m], s);
        }
    }
}


function toastContainer() {
    let toastContainer = document.getElementById('unicaen-vue-toast-container');
    if (!toastContainer) {
        toastContainer = document.createElement('div');
        toastContainer.id = 'unicaen-vue-toast-container';
        toastContainer.classList.add('toast-container', 'position-fixed', 'top-0', 'end-0', 'p-3');
        document.body.appendChild(toastContainer);
    }
    return toastContainer;
}


function prepareMessage(message) {
    message = removeAlert(message);
    message = removeIcon(message);

    return message;
}


function removeAlert(message) {
    const el = document.createElement("div");
    el.innerHTML = message;

    // Si on trouve une alert, alors on la retire pour éviter de faire double affichage
    const alert = el.querySelector('.alert');
    if (alert) {
        return alert.innerHTML;
    } else {
        return el.innerHTML;
    }
}


function removeIcon(message) {
    const el = document.createElement("div");
    el.innerHTML = message;

    // on masque l'icône /!\ qui fait doublon si on en trouve
    const fasIcons = el.querySelectorAll('i.fas');
    fasIcons.forEach(icon => icon.style.display = "none");

    return el.innerHTML;
}


function toast(message, severity, options = null) {
    const bgClasses = {
        info: 'bg-info',
        success: 'bg-success',
        warning: 'bg-warning',
        error: 'bg-danger'
    };
    const iconClasses = {
        info: 'info-circle',
        success: 'check-circle',
        warning: 'exclamation-circle',
        error: 'exclamation-triangle'
    };

    // Crée le bouton de fermeture
    const closeButton = document.createElement('button');
    closeButton.classList.add('btn-close', 'btn-close-white', 'h5');
    closeButton.style.float = 'right';
    closeButton.setAttribute('data-bs-dismiss', 'toast');
    closeButton.setAttribute('aria-label', 'Close');

    // Crée l'icône
    const icon = document.createElement('i');
    icon.classList.add('icon', 'fas', `fa-${iconClasses[severity]}`);
    icon.style.float = 'left';
    icon.style.fontSize = '26pt';
    icon.style.paddingLeft = '.4rem';
    icon.style.marginTop = '.4rem';
    icon.style.paddingRight = '1rem';

    // Crée le corps du toast
    const body = document.createElement('div');
    body.classList.add('toast-body');
    body.innerHTML = prepareMessage(message);

    // Crée le contenu du toast en ajoutant les éléments créés précédemment
    const content = document.createElement('div');
    content.appendChild(closeButton);
    content.appendChild(icon);
    content.appendChild(body);

    // Création de l'élément HTML pour le toast
    const toast = document.createElement('div');
    toast.classList.add('toast', 'text-white', bgClasses[severity] ? bgClasses[severity] : 'bg-secondary');
    toast.setAttribute('role', 'alert');
    toast.setAttribute('aria-live', 'assertive');
    toast.setAttribute('aria-atomic', 'true');

    // On élargit le toast pour ahhicher tout ça...
    if (severity === 'error' && message.length > 500) {
        toast.setAttribute('style', 'width:700px');
    }
    toast.appendChild(content);

    // Ajout du toast à l'élément du conteneur de toasts
    toastContainer().appendChild(toast);

    // Création et affichage du toast avec bootstrap
    if(options == null)
    {
        options = {
            animation: true,
            delay: severity === 'warning' ? 15000 : 5000,
            autohide: severity !== 'error'
        };
    }

    let bsToast = new bootstrap.Toast(toast, options);
    bsToast.show();
}


export default {
    toast,
    toasts
}