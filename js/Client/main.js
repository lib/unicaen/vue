import {createApp} from 'vue';
import {createBootstrap} from 'bootstrap-vue-next';
import unicaenVue from './unicaenVue';

function init(vues, options)
{
    const components = {};

    if (undefined === options) {
        options = {};
    }

    // Convertir les chemins des composants en noms de composants
    for (const path in vues) {
        let compPath = path.slice(2, -4);
        let compName = compPath.replaceAll('/', '');
        components[compName] = vues[path].default;
    }

    // Fonction pour initialiser une application Vue sur un élément
    function mountVueApp(el)
    {
        let app = createApp({
            template: el.innerHTML,
            components: components
        });

        if (undefined !== options.beforeMount) {
            options.beforeMount(app);
        }

        // Autoload des composants déclarés
        if (undefined !== options.autoloads) {
            for (const alias in options.autoloads) {
                let compName = options.autoloads[alias].replaceAll('/', '');
                app.component(alias, components[compName]);
            }
        }

        app.use(createBootstrap({components: true, directives: true}));
        app.mount(el);

        if (undefined !== options.afterMount) {
            options.afterMount(app);
        }
    }

    // Initialiser les applications Vue sur les éléments existants
    for (const el of document.getElementsByClassName('vue-app')) {
        mountVueApp(el);
    }

    // Observer les changements du DOM pour détecter les nouveaux éléments chargés en AJAX
    const observer = new MutationObserver((mutationsList) => {
        for (const mutation of mutationsList) {
            if (mutation.type === 'childList') {
                // Parcourir les nœuds ajoutés
                for (const node of mutation.addedNodes) {
                    // Vérifier si le nœud est un élément et contient des éléments .vue-app
                    if (node.nodeType === Node.ELEMENT_NODE) {
                        // Rechercher des éléments .vue-app dans le nœud ajouté et ses enfants
                        const vueApps = node.querySelectorAll ? node.querySelectorAll('.vue-app') : [];
                        for (const el of vueApps) {
                            mountVueApp(el);
                        }

                        // Vérifier si le nœud lui-même est un .vue-app
                        if (node.classList.contains('vue-app')) {
                            mountVueApp(node);
                        }
                    }
                }
            }
        }
    });

    // Démarrer l'observation du DOM
    observer.observe(document.body, {
        childList: true, // Observer les ajouts/suppressions d'enfants
        subtree: true,   // Observer tout le sous-arbre du DOM
    });
}

export default {
    init
};