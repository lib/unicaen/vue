import axios from './axios';
import flashMessenger from './flashMessenger';

const unicaenVue = {
    axios: axios.uvAxios,
    flashMessenger: flashMessenger,

    /**
     * Crée une URL à partie d'une route, de paramètres de route (params) et de paramètres GET (query)
     *
     * @param String route
     * @param Object params
     * @param Object query
     * @returns String
     */
    url: (route, params, query) => {
        let baseUrl = window.__unicaenVueBaseUrl;
        // Remplacement des paramètres de routes par leurs valeurs
        if (params) {
            for (let p in params) {
                route = route.replace(`:${p}`, params[p]);
            }
        }

        // traitement de la requête GET
        let getArgs = query ? unicaenVue.encodeUrlQueryParam(query) : null;

        // Construction et retour de l'URL
        return baseUrl + route + (getArgs ? `?${getArgs}` : '');
    },

    encodeUrlQueryParam: (query, parentKey) => {
        var params = [];

        for (var key in query) {
            if (query.hasOwnProperty(key)) {
                var value = query[key];
                var encodedKey = parentKey ? parentKey + '[' + encodeURIComponent(key) + ']' : encodeURIComponent(key);

                if (typeof value === 'object' && value !== null) {
                    params.push(unicaenVue.encodeUrlQueryParam(value, encodedKey));
                } else {
                    var encodedValue = encodeURIComponent(value);
                    params.push(encodedKey + '=' + encodedValue);
                }
            }
        }

        return params.join('&');
    }

};


// workaround pour rendre unicaenVue disponible de partout
window.unicaenVue = unicaenVue;

export default {
    unicaenVue
}