const path = require("path");

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
}


/**
 * Permet de fusionner des objets JSON comprenant des sous-objets ou des tableaux
 * @param target
 * @param ...sources
 * @returns Object
 */
function deepMerge(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, {[key]: {}});
                deepMerge(target[key], source[key]);
            } else if (Array.isArray(target[key]) && Array.isArray(source[key])) {
                for (const subKey in source[key]) {
                    const val = source[key][subKey];
                    if (-1 === target[key].indexOf(val)) {
                        target[key].push(val);
                    }
                }
            } else {
                Object.assign(target, {[key]: source[key]});
            }
        }
    }

    return deepMerge(target, ...sources);
}


/**
 * Retourne le répertoire racine d'UnicaenVue
 *
 * @returns String
 */
function unicaenVueDir() {
    const path = require('path');

    // répertoire racine d'UnicaenVue', en partant du répertoire actuel, donc src/Server
    return path.resolve(__dirname, '../../');
}


/**
 * Retourne le répertoire racine de l'application.
 *
 * @returns String
 */
function projectDir() {
    // répertoire racine de l'application
    // Attention : le script doit être lancé depuis le bon répertoire...
    return process.cwd();
}


/**
 * Retourne la liste des composants Vue dans un répertoire donné par nom de composant et fichiers en chemin absolu
 *
 * Exemple de retour pour les fichiers suivants (chemin absolu) :
 * - /var/www/html/prod/monAppli/components/MonComposant.vue
 * - /var/www/html/prod/monAppli/components/MonComposant2.vue
 * - /var/www/html/prod/monAppli/components/Autres/MonAutre3.vue
 *
 * findComponents('/var/www/html/prod/monAppli/components') => {
 *     MonComposant: /var/www/html/prod/monAppli/components/MonComposant.vue,
 *     MonComposant2: /var/www/html/prod/monAppli/components/MonComposant2.vue,
 *     AutresMonAutre3: /var/www/html/prod/monAppli/components/Autres/MonAutre3.vue
 * }
 *
 * @param string componentsDir
 * @returns {{}}
 */
function findComponents(componentsDir) {
    const path = require("path");
    const FS = require("fs");

    let componentsFiles = [];

    function parseComponents(Directory) {
        FS.readdirSync(Directory).forEach(File => {
            const absolute = path.join(Directory, File);
            if (FS.statSync(absolute).isDirectory()) return parseComponents(absolute);
            else if (absolute.endsWith('.vue')) {
                return componentsFiles.push(absolute.slice(componentsDir.length + 1, -4));
            }
        });
    }

    parseComponents(componentsDir);


    let components = {};

    for (const c in componentsFiles) {
        components[componentsFiles[c].replaceAll('/', '')] = path.resolve(componentsDir, componentsFiles[c]) + '.vue';
    }

    return components;
}


/**
 *
 * @param Object projectConfig
 * @returns {*}
 */
function defineConfig(projectConfig) {
    const path = require('path');
    const vue = require('@vitejs/plugin-vue');
    const vite = require('vite');
    const liveReload = require('vite-plugin-live-reload');
    const Components = require('unplugin-vue-components/vite');
    const componentsDir = path.resolve(__dirname, '../../components');
    const components = findComponents(componentsDir);
    const resolvers = require('unplugin-vue-components/resolvers');

    const root = projectConfig.root ? projectConfig.root : 'root';

    /** @see https://vitejs.dev/config/
     * Configuration par défaut de Vite.
     * Celle-ci pourra être surchargée par la configuration issue de l'application et fournie via projectConfig
     */
    const unicaenVueConfig = {
        plugins: [
            // Démarre Vue
            vue(),

            // hot-loading pour tous les CSS du répertoire public du projet en bonus
            liveReload.liveReload([path.resolve(projectDir(), 'public/**/*.css')]),

            vite.splitVendorChunkPlugin()
        ],
        root: root,
        build: {
            // output dir for production build
            outDir: path.resolve(__dirname, 'public/dist'),
            // On vide le répertoire avant de rebuilder
            emptyOutDir: true,
            // Publication du manifest oboigatoire
            manifest: true,
            // fichier de démarrage
            rollupOptions: {
                input: path.resolve(projectDir(), root, 'main.js'),
            },
            // chunkSizeWarningLimit augmenté : le vendor peut faire 1Mo (1024Ko)
            chunkSizeWarningLimit: 1024
        },
        server: {
            // specs pour le serveur Node
            strictPort: true,
            port: 5133,
            cors: true,
        },
        resolve: {
            alias: {
                vue: 'vue/dist/vue.esm-bundler.js',
                '@': path.resolve(projectDir(), root)
            }
        },
        resolvers: [
            (componentName) => {
                if (components[componentName] !== undefined) {
                    return components[componentName];
                }
            },
            resolvers.BootstrapVueNextResolver()
        ],
    };

    const finalConfig = deepMerge(unicaenVueConfig, projectConfig);

    finalConfig.plugins.push(
        Components({resolvers: finalConfig.resolvers})
    );

    //console.log(finalConfig);
    return vite.defineConfig(finalConfig);
}


function start() {

}


module.exports = {
    defineConfig,
    projectDir,
    findComponents,
    unicaenVueDir,
    start
};