<?php

namespace UnicaenVue\Axios;


interface AxiosExtractorInterface
{

    /**
     * @return array
     */
    public function axiosDefinition(): array;

}