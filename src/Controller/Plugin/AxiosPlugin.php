<?php

namespace UnicaenVue\Controller\Plugin;

use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

/**
 * Description of Axios
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class AxiosPlugin extends AbstractPlugin
{
    public function fromPost(?string $param = null, $default = null)
    {
        $post = json_decode(file_get_contents('php://input'), true);
        if ($param) {
            if (array_key_exists($param, $post)) {
                return $post[$param];
            } else {
                return $default;
            }
        } else {
            return $post;
        }
    }
}