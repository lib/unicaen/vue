<?php

namespace UnicaenVue;

use Doctrine\ORM\EntityManager;
use UnicaenVue\View\Model\AxiosModel;

class Util
{
    static public function tableAjaxData(EntityManager $em, array $post, string $sql): AxiosModel
    {
        $search = $post['search'] ?? '';
        $elStart = ($post['elStart'] ?? 1) - 1;
        $size = $post['size'] ?? 10;
        $orderCol = $post['orderCol'] ?? null;
        $orderDir = ($post['orderDir'] ?? 'asc') == 'asc' ? 'asc' : 'desc';

        if (!is_int($size)){
            $size = 10000;
        }
        
        if ($orderCol && (str_contains($orderCol, '"') || str_contains($orderCol, "'"))){
            $orderCol = null; // protection contre les injections
        }

        $limitSql = "OFFSET :elStart ROWS FETCH FIRST :size ROWS ONLY";
        if ($orderCol) {
            $orderSql = "ORDER BY $orderCol $orderDir";
        } else {
            $orderSql = '';
        }
        $params = [
            'elStart' => $elStart,
            'size'    => $size,
            'search'  => strtolower('%' . $search . '%'),
        ];
        $count = (int)$em->getConnection()->fetchOne('SELECT count(*) cc FROM (' . $sql . ') t', $params);
        $data = $em->getConnection()->fetchAllAssociative($sql . "\n" . $orderSql . "\n" . $limitSql, $params);

        $result = [
            'count' => $count,
            'data'  => $data,
        ];

        return new AxiosModel($result);
    }
}