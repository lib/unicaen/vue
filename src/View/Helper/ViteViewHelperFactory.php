<?php

namespace UnicaenVue\View\Helper;

use Psr\Container\ContainerInterface;


/**
 * Description of ViteViewHelperFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class ViteViewHelperFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return ViteViewHelper
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): ViteViewHelper
    {
        $config = $container->get('config');

        $viteConfig                = $config['unicaen-vue'] ?? [];
        $viteConfig['host']        = $viteConfig['host'] ?? 'http://localhost:5133';
        $viteConfig['hot-loading'] = $viteConfig['hot-loading'] ?? false;
        $viteConfig['dist-path']   = $viteConfig['dist-path'] ?? 'public/dist';
        $viteConfig['dist-url']   = $viteConfig['dist-url'] ?? 'dist';

        $viewHelper = new ViteViewHelper($viteConfig);

        return $viewHelper;
    }
}