<?php

namespace UnicaenVue\View\Model;

use Laminas\View\Model\ModelInterface;
use Traversable;

class AxiosModel implements ModelInterface
{
    protected $data = null;

    protected bool $returnFirstItem = false;

    protected array $properties = [];

    protected array $triggers = [];



    public function __construct($data = null, array $properties = [], array $triggers = [])
    {
        $this->data = $data;
        $this->properties = $properties;
        $this->triggers = $triggers;
    }



    public function getData()
    {
        return $this->data;
    }



    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }



    public function getProperties(): array
    {
        return $this->properties;
    }



    public function setProperties(array $properties): AxiosModel
    {
        $this->properties = $properties;

        return $this;
    }



    public function getTriggers(): array
    {
        return $this->triggers;
    }



    public function setTriggers(array $triggers): AxiosModel
    {
        $this->triggers = $triggers;

        return $this;
    }



    public function returnFirstItem(bool $returnFirstItem = true)
    {
        $this->returnFirstItem = $returnFirstItem;
    }



    public function isReturnFirstItem(): bool
    {
        return $this->returnFirstItem;
    }



    public function getVariables()
    {
        return $this->data;
    }



    public function setVariables($data)
    {
        $this->data = $data;

        return $this;
    }



    public function getOptions(): array
    {
        return $this->properties;
    }



    public function setOptions($options)
    {
        $this->properties = $options;

        return $this;
    }



    public function setOption($name, $value)
    {
        throw new \Exception('setOption is not available with AxiosModel');
    }



    public function getVariable($name, $default = null)
    {
        throw new \Exception('getVariable is not available with AxiosModel');
    }



    public function setVariable($name, $value)
    {
        throw new \Exception('setVariable is not available with AxiosModel');
    }



    public function setTemplate($template)
    {
        // do nothing, no templates needed
    }



    public function getTemplate()
    {
        // do nothing, no templates needed
    }



    public function addChild(ModelInterface $child, $captureTo = null, $append = false)
    {
        // no nothing : AxiosModel does not support children
    }



    public function getChildren()
    {
        return [];
    }



    public function hasChildren()
    {
        return false;
    }



    public function setCaptureTo($capture)
    {
        throw new \Exception('setCaptureTo is not available with AxiosModel');
    }



    public function captureTo()
    {
        throw new \Exception('captureTo is not available with AxiosModel');
    }



    public function setTerminal($terminate)
    {
        throw new \Exception('setTerminal is not available with AxiosModel');
    }



    public function terminate()
    {
        return true;
    }



    public function setAppend($append)
    {
        throw new \Exception('setAppend is not available with AxiosModel');
    }



    public function isAppend()
    {
        throw new \Exception('isAppend is not available with AxiosModel');
    }



    public function getIterator(): Traversable
    {
        throw new \Exception('getIterator is not available with AxiosModel');
    }



    public function count(): int
    {
        return 0;
    }

}