<?php

namespace UnicaenVue\View\Model;

use Laminas\View\Model\ModelInterface;
use Laminas\View\Model\ViewModel;

class VueModel extends ViewModel
{

    /**
     * Add a child model
     *
     * @param null|string $captureTo Optional; if specified, the "capture to" value to set on the child
     * @param null|bool   $append    Optional; if specified, append to child  with the same capture
     *
     * @return ViewModel
     */
    public function addChild(ModelInterface $child, $captureTo = null, $append = null)
    {
        throw new \Exception('Les VueModel ne prennent pas en charge les enfants!');
    }
}