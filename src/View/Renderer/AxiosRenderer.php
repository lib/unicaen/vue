<?php

namespace UnicaenVue\View\Renderer;


use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\JsonModel;
use Laminas\View\Renderer\JsonRenderer;
use UnicaenVue\Axios\AxiosExtractor;
use UnicaenVue\View\Model\AxiosModel;

/**
 * Description of AxiosRenderer
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class AxiosRenderer extends JsonRenderer
{
    protected FlashMessenger $flashMessenger;



    public function render($nameOrModel, $values = null)
    {
        if ($nameOrModel instanceof AxiosModel) {
            $data = $this->makeData($nameOrModel);
        }

        return parent::render($data, null);
    }



    protected function makeData(AxiosModel $axiosModel): array
    {
        $data = AxiosExtractor::extract($axiosModel->getData(), $axiosModel->getProperties(), $axiosModel->getTriggers());

        if (is_array($data) && $axiosModel->isReturnFirstItem()){
            $data = reset($data);
        }

        $namespaces = [
            $this->flashMessenger::NAMESPACE_SUCCESS,
            $this->flashMessenger::NAMESPACE_WARNING,
            $this->flashMessenger::NAMESPACE_ERROR,
            $this->flashMessenger::NAMESPACE_INFO,
        ];

        $messages = [];
        foreach ($namespaces as $namespace) {
            if ($this->flashMessenger->hasCurrentMessages($namespace)) {
                $messages[$namespace] = $this->flashMessenger->getCurrentMessages($namespace);
                $this->flashMessenger->clearCurrentMessages($namespace);
            }
        }

        $jsonData = [
            'data'     => $data,
            'messages' => $messages,
        ];

        return $jsonData;
    }



    public function getFlashMessenger(): FlashMessenger
    {
        return $this->flashMessenger;
    }



    public function setFlashMessenger(FlashMessenger $flashMessenger): AxiosRenderer
    {
        $this->flashMessenger = $flashMessenger;

        return $this;
    }

}