<?php

namespace UnicaenVue\View\Renderer;

use Psr\Container\ContainerInterface;



/**
 * Description of AxiosRendererFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class AxiosRendererFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return AxiosRenderer
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): AxiosRenderer
    {
        $renderer = new AxiosRenderer();

        $flashMessenger = $container->get('ControllerPluginManager')->get('flashMessenger');
        $renderer->setFlashMessenger($flashMessenger);

        return $renderer;
    }
}