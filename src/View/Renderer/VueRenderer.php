<?php

namespace UnicaenVue\View\Renderer;

use Laminas\Filter\FilterChain;
use Laminas\View\Helper\ViewModel;
use Laminas\View\Model\ModelInterface;
use Laminas\View\Model\ModelInterface as Model;
use Laminas\View\Renderer\PhpRenderer;
use UnicaenVue\View\Helper\VueViewHelper;
use UnicaenVue\View\Model\VueModel;

/**
 * @author Laurent LÉCLUSE <laurent.lecluse at unicaen.fr>
 */
class VueRenderer extends PhpRenderer
{

    /**
     * Processes a view script and returns the output.
     *
     * @param VueModel               $vueModel
     * @param null|array|Traversable $values Values to use when rendering. If none
     *                                       provided, uses those in the composed
     *                                       variables container.
     *
     * @return string The script output.
     */
    public function render($vueModel, $values = null)
    {
        if ($vueModel instanceof VueModel) {

            /** @var VueViewHelper $vue */
            $vue       = $this->getHelperPluginManager()->get('vue');
            $variables = $vueModel->getVariables();

            $content = $vue->begin();
            $content .= $vue->component($vueModel->getTemplate(), (array)$vueModel->getVariables());
            $content .= $vue->end();

            return $content;
        } else {
            return parent::render($vueModel, $values);
        }
    }
}
