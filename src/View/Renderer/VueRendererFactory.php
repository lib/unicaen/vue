<?php

namespace UnicaenVue\View\Renderer;

use Psr\Container\ContainerInterface;


/**
 * Description of VueRendererFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class VueRendererFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return VueRenderer
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): VueRenderer
    {
        $renderer = new VueRenderer();
        $renderer->setHelperPluginManager($container->get('ViewHelperManager'));
        $renderer->setResolver($container->get('ViewResolver'));

        return $renderer;
    }
}