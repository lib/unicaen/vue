<?php

namespace UnicaenVue\View\Strategy;

use Psr\Container\ContainerInterface;
use UnicaenVue\View\Renderer\AxiosRenderer;


/**
 * Description of AxiosStrategyFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class AxiosStrategyFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return AxiosStrategy
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): AxiosStrategy
    {
        $renderer = $container->get(AxiosRenderer::class);

        $strategy = new AxiosStrategy($renderer);

        return $strategy;
    }
}