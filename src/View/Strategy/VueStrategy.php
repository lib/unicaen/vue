<?php

namespace UnicaenVue\View\Strategy;

use Laminas\EventManager\AbstractListenerAggregate;
use Laminas\EventManager\EventManagerInterface;
use Laminas\View\ViewEvent;
use UnicaenVue\View\Model\VueModel;
use UnicaenVue\View\Renderer\VueRenderer;

class VueStrategy extends AbstractListenerAggregate
{
    protected VueRenderer $renderer;



    public function __construct(VueRenderer $renderer)
    {
        $this->renderer = $renderer;
    }



    /**
     * Detect if we should use the CsvRenderer based on model type and/or
     * Accept header
     *
     * @param ViewEvent $e
     *
     * @return null|VueRenderer
     */
    public function selectRenderer(ViewEvent $e)
    {
        $model = $e->getModel();

        if (!$model instanceof VueModel) {
            // no VueModel; do nothing
            return;
        }

        // VueModel found
        return $this->renderer;
    }



    /**
     * @param EventManagerInterface $events
     * @param                       $priority
     *
     * @return void
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(ViewEvent::EVENT_RENDERER, [$this, 'selectRenderer'], $priority);
        $this->listeners[] = $events->attach(ViewEvent::EVENT_RESPONSE, [$this, 'injectResponse'], $priority);
    }



    /**
     * Inject the response with the Vue payload and appropriate Content-Type header
     *
     * @param ViewEvent $e
     *
     * @return void
     */
    public function injectResponse(ViewEvent $e)
    {
        $renderer = $e->getRenderer();
        if ($renderer !== $this->renderer) {
            // Discovered renderer is not ours; do nothing
            return;
        }

        $result = $e->getResult();
        if (!is_string($result)) {
            // We don't have a string, and thus, no HTML
            return;
        }

        // Populate response
        $response = $e->getResponse();
        $response->setContent($result);
        $headers = $response->getHeaders();
    }

}