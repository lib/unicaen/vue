<?php

namespace UnicaenVue\View\Strategy;

use Psr\Container\ContainerInterface;
use UnicaenVue\View\Renderer\VueRenderer;


/**
 * Description of VueViewHelperFactory
 *
 * @author Laurent Lécluse <laurent.lecluse at unicaen.fr>
 */
class VueStrategyFactory
{

    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return VueViewHelper
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null): VueStrategy
    {
        $renderer = $container->get(VueRenderer::class);

        $strategy = new VueStrategy($renderer);

        return $strategy;
    }
}